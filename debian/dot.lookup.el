;;; .lookup.el --- sample of ~/.lookup.el  -*-mode: emacs-lisp;-*-

;; Author: Tatsuya Kinoshita <tats@vega.ocn.ne.jp>

;; Unlimited permission is granted to use, copy, distribute and/or modify
;; this file.  There is NO WARRANTY.

;;; Commentary:

;; The following is a sample of ~/.emacs.
;;
;; (autoload 'my-lookup-word "lookup" nil t)
;; (global-set-key "\C-cl" 'my-lookup-word)
;; (global-set-key "\C-cL" 'lookup)
;; (setq lookup-enable-splash nil)

;;; Code:

(defun my-lookup-word (&optional arg)
  "Read a word from the minibuffer for Lookup.
The current word is used by default.
With \\[universal-argument], the current region is used."
  (interactive "P")
  (if arg
      (my-lookup-region)
    (my-lookup-read-from-minibuffer (lookup-current-word))))

(defun my-lookup-region ()
  "Read a word from the minibuffer for Lookup.
The current region is used by default."
  (interactive)
  (my-lookup-read-from-minibuffer
   (buffer-substring-no-properties (or (mark) (point)) (point))))

(defun my-lookup-read-from-minibuffer (&optional string)
  (if (null string) (setq string ""))
  (while (string-match "[\n\t]+" string)
    (setq string (replace-match " " nil t string)))
  (while (string-match "  +" string)
    (setq string (replace-match " " nil t string)))
  (while (string-match "\\(^ \\| $\\)" string)
    (setq string (replace-match "" nil t string)))
  (lookup-pattern (lookup-read-string "Look up" string 'lookup-input-history)))

(setq lookup-default-method 'prefix) ;; Default value is 'exact
(setq lookup-default-dictionary-options '((:stemmer . stem-english)))

;;;(setq lookup-search-agents
;;;      '(
;;;	(ndeb "/usr/local/share/dict/epwing/XXX" ;; Copy from EPWING CD-ROM
;;;	      ;; To create an appendix data, use eb-utils' ebappendix command.
;;;	      ;; See also http://www.sra.co.jp/people/m-kasahr/eb/appendix.html
;;;	      :appendix "/usr/local/share/dict/epwing-appendix/XXX"
;;;	      :alias "XXX")
;;;	(ndeb "/usr/share/dict/epwing/edict" ;; edict-fpw
;;;	      :alias "edict")
;;;	(ndict "localhost" :coding utf-8-dos) ;; dictd and dictd-dictionary
;;;	(ndic "/usr/share/dictd") ;; without dictd, slow, UTF-8 is unsupported
;;;	(ndtp "localhost") ;; ndtpd
;;;	(ndkks) ;; kakasi
;;;	(ndspell) ;; ispell
;;;	;;
;;;	))

;;;(setq lookup-search-modules
;;;      '(
;;;	("default"
;;;	 "ndeb+XXX:YYY"
;;;	 "ndeb+edict:"
;;;	 "ndict+localhost:jargon"
;;;	 "ndict+localhost:foldoc"
;;;	 "ndict+localhost:vera"
;;;	 ;;
;;;	 )
;;;	("all" "")
;;;	;;
;;;	))

;;; .lookup.el ends here
